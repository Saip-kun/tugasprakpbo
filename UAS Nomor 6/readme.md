# WEBSERVICE REST-API
## PENJELASAN
REST API (Representational State Transfer Application Programming Interface) adalah suatu gaya arsitektur perangkat lunak yang digunakan untuk merancang layanan web yang dapat berkomunikasi satu sama lain melalui protokol HTTP. REST API berfokus pada sederhananya interaksi antara klien (seperti aplikasi web, perangkat lunak, atau perangkat lain) dengan server yang menyediakan data atau layanan.

Prinsip utama REST API adalah:

1. Sumber Daya (Resources): Sumber daya (resources) merupakan entitas yang diidentifikasi oleh URL. Sumber daya dapat berupa objek, data, atau layanan yang ingin diakses atau dimanipulasi.

2. Metode HTTP: REST API menggunakan metode HTTP untuk berkomunikasi dengan server. Metode HTTP yang umum digunakan adalah GET (untuk membaca data), POST (untuk membuat data baru), PUT (untuk memperbarui data), dan DELETE (untuk menghapus data).

3. Representasi Data: Data dalam REST API biasanya direpresentasikan dalam format yang umum seperti JSON (JavaScript Object Notation) atau XML (eXtensible Markup Language).

4. Stateless: REST API bersifat stateless, artinya setiap permintaan yang dikirim ke server harus mencakup semua informasi yang diperlukan. Server tidak menyimpan status klien sebelumnya, sehingga setiap permintaan dianggap mandiri.

5. Tautan (Links): REST API menggunakan tautan untuk menghubungkan sumber daya yang terkait. Tautan ini memungkinkan klien untuk menavigasi antara sumber daya dan mengakses informasi tambahan terkait.

Dalam praktiknya, untuk menggunakan REST API, klien akan mengirim permintaan HTTP ke server dengan URL yang sesuai, menggunakan metode HTTP yang tepat, dan mungkin menyertakan data dalam format yang diperlukan. Server akan memproses permintaan tersebut dan memberikan respon HTTP kembali ke klien, sering kali dengan data yang diminta atau status yang mengindikasikan apakah permintaan berhasil atau gagal.

REST API telah menjadi standar industri yang umum digunakan untuk menghubungkan sistem dan memungkinkan berbagai aplikasi dan layanan berinteraksi dan berbagi data secara efisien.

## SCREENSHOOTS
![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-10%20083353.png )
![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-11%20082745.png )
![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-07%20131057.png )
![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-11%20082527.png )
