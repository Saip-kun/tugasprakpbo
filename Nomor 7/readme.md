# Menerjemahkan Proses Bisnis ke dalam Skema OOP
## Use Case Diagram
![image.png](https://gitlab.com/Saip-kun/TugasPrakBasisData/-/raw/main/UAS%20Nomor%203/Untitled_Diagram.png)

Dalam Diagram Use Case ini terdapat dua aktor utama, yaitu admin dan user. Mari kita deskripsikan masing-masing aktor dalam OOP.

1. Admin:
- Objek: Admin
- Atribut: username, password, profile
- Perilaku (metode): login(), updateProfile(), diskusiProduk(), mengelolaUser(), updateInformasi(), buatLaporan(), updateBarang()

2. User:
- Objek: User
- Atribut: username, password, profile
- Perilaku (metode): register(), login(), updateProfile(), diskusiProduk(), lihatKatalog(), pemesanan(), pilihBarang(), pembayaran()

Dalam desain ini, terdapat dua kelas, yaitu Admin dan User, yang mewakili aktor-aktor dalam sistem. Setiap kelas memiliki atribut-atribut yang relevan dengan aktor tersebut, seperti username, password, dan profile. Selain itu, setiap kelas juga memiliki metode-metode yang mewakili tindakan-tindakan yang dapat dilakukan oleh aktor tersebut, seperti registrasi(), login(), updateProfile(), dan diskusiProduk().

Selain kelas-kelas tersebut, terdapat kelas-kelas tambahan yang mewakili entitas-entitas lain dalam proses bisnis, seperti Produk, Katalog, Pemesanan, dan Pembayaran. Anda dapat mengidentifikasi atribut-atribut dan metode-metode yang relevan untuk setiap kelas tambahan tersebut.
