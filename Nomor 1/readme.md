# Mendemonstrasikan Penyelesaian Masalah dengan Pendekatan Matematika dan Algoritma Pemrograman
## Penyelesaian Masalah menggunakan Quick-Sort
<pre><code>import java.util.*;
 
public class quicksort{
    public static void swap (int A[], int x, int y){
        int temp = A[x];
        A[x] = A[y];
        A[y] = temp;
    }
    public static int partition(int A[], int f, int l){
        int pivot = A[f];
        while (f < l){
            while (A[f] < pivot) f++;
            while (A[l] > pivot) l--;
            swap (A, f, l);
        }
        return f;
    } public static void Quicksort(int A[], int f, int l){
        if (f >= l) return;
        int pivotIndex = partition(A, f, l);
        Quicksort(A, f, pivotIndex);
        Quicksort(A, pivotIndex+1, l);
    }public static void main(String argv[]){
        int []numbers={55,2,93,1,23,10,66,12,7,54,3};
        System.out.println(Arrays.toString(numbers));
        Quicksort(numbers, 0, numbers.length-1);
        System.out.println(Arrays.toString(numbers));
    }
}</pre></code>

Quicksort adalah algoritma pengurutan yang efisien dengan strategi pemisahan dan penyelesaian rekursif.

Pada kodingan tersebut, array `numbers` awalnya berisi angka yang tidak terurut. Setelah menjalankan algoritma Quicksort, array `numbers` akan terurut secara menaik. Berikut adalah penjelasan langkah-langkah algoritma Quicksort dan output yang dihasilkan:

1. Fungsi `swap(int A[], int x, int y)` digunakan untuk menukar posisi elemen dalam array `A`.
2. Fungsi `partition(int A[], int f, int l)` digunakan untuk membagi array `A` menjadi dua bagian dengan menggunakan pivot (elemen awal). Angka yang lebih kecil dari pivot akan ditempatkan di sebelah kiri, dan angka yang lebih besar akan ditempatkan di sebelah kanan.
3. Fungsi `Quicksort(int A[], int f, int l)` adalah implementasi rekursif algoritma Quicksort. Jika indeks pertama (`f`) lebih besar atau sama dengan indeks terakhir (`l`), maka array tersebut sudah terurut, sehingga fungsi tersebut akan berhenti. Jika tidak, fungsi akan memanggil dirinya sendiri untuk membagi array menjadi dua bagian, kemudian melakukan pengurutan pada masing-masing bagian.
4. Pada fungsi `main`, array `numbers` diinisialisasi dengan beberapa angka yang tidak terurut. Kemudian, dilakukan pencetakan array tersebut sebelum dilakukan pengurutan dengan menggunakan `System.out.println(Arrays.toString(numbers))`.
5. Setelah menjalankan fungsi `Quicksort` dengan parameter array `numbers` dan indeks awal dan akhir, array tersebut akan terurut secara menaik. Dilakukan pencetakan array `numbers` yang sudah terurut menggunakan `System.out.println(Arrays.toString(numbers))`.

Output yang dihasilkan adalah sebagai berikut:

```
[55, 2, 93, 1, 23, 10, 66, 12, 7, 54, 3]
[1, 2, 3, 7, 10, 12, 23, 54, 55, 66, 93]
```

Output pertama adalah array `numbers` sebelum dilakukan pengurutan, sedangkan output kedua adalah array `numbers` setelah menjalankan algoritma Quicksort, di mana elemen-elemennya sudah terurut secara menaik.

## Penyelesaian Masalah menggunakan Shell-Sort
<pre><code>import java.util.*;
public class shellsort{
    public static void sort(int[]array)
    {
        int inner, outer;
        int temp;
        int h=1;
        while(h <= array.length /3)
        {
            h=h*3+1;
        }
        while(h>0)
        {
            for(outer=h; outer<array.length;outer++)
            {
                temp=array[outer];
                inner=outer;
                while(inner>h-1&&array[inner-h]>=temp)
                {
                    array[inner]=array[inner-h];
                    inner-=h;
                }
                array[inner]=temp;
            }
            h=(h-1)/3;
        }
    }
    public static void main(String[] args){
        int [] array = {5,3,0,2,4,1,0,5,2,3,1,4}; 
        System.out.println("Before: " + Arrays.toString(array));
        sort(array);
        System.out.println("After:  " + Arrays.toString(array));
    }
}</pre></code>

Shell Sort adalah algoritma pengurutan yang merupakan variasi dari Insertion Sort dengan menggunakan strategi pengurutan secara bertahap dengan jarak tertentu.

Pada kodingan tersebut, array `array` awalnya berisi angka yang tidak terurut. Setelah menjalankan algoritma Shell Sort, array `array` akan terurut secara menaik. Berikut adalah penjelasan langkah-langkah algoritma Shell Sort dan output yang dihasilkan:

1. Fungsi `sort(int[] array)` adalah implementasi algoritma Shell Sort. Algoritma ini menggunakan konsep interval (disebut juga gap) yang secara bertahap diperkecil hingga mencapai jarak interval 1.
2. Pada awalnya, nilai gap `h` diinisialisasi dengan 1. Kemudian, gap `h` akan diperbesar dengan rumus `h = h * 3 + 1` hingga memenuhi syarat `h <= array.length / 3`.
3. Selanjutnya, algoritma akan melakukan iterasi dengan gap `h` pada array. Setiap iterasi, elemen yang terletak pada posisi `outer` akan dibandingkan dengan elemen pada posisi `inner` yang terletak pada jarak `h` sebelumnya. Jika elemen pada posisi `inner` lebih besar, maka elemen tersebut akan dipindahkan ke posisi `inner + h`.
4. Proses pemindahan elemen dilakukan secara berulang hingga elemen pada posisi `inner` lebih kecil atau sama dengan elemen yang sedang dibandingkan (`temp`).
5. Setelah selesai melakukan pemindahan elemen dengan gap `h`, gap `h` akan diperkecil dengan rumus `(h - 1) / 3`.
6. Proses langkah 3-5 akan diulang hingga gap `h` mencapai nilai 1.
7. Pada fungsi `main`, array `array` diinisialisasi dengan beberapa angka yang tidak terurut. Kemudian, dilakukan pencetakan array tersebut sebelum dilakukan pengurutan dengan menggunakan `System.out.println("Before: " + Arrays.toString(array))`.
8. Setelah menjalankan fungsi `sort` dengan parameter array `array`, array tersebut akan terurut secara menaik. Dilakukan pencetakan array `array` yang sudah terurut menggunakan `System.out.println("After:  " + Arrays.toString(array))`.

Output yang dihasilkan adalah sebagai berikut:

```
Before: [5, 3, 0, 2, 4, 1, 0, 5, 2, 3, 1, 4]
After:  [0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5]
```

Output pertama adalah array `array` sebelum dilakukan pengurutan, sedangkan output kedua adalah array `array` setelah menjalankan algoritma Shell Sort, di mana elemen-elemennya sudah terurut secara menaik.


