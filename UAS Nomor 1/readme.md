# Use Case Tokopedia beserta ranking
## Diagram Use Case
![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Untitled%20Diagram.png )

## Berikut adalah keseluruhan Use Case dari Tokopedia beserta ranking untuk masing-masing Use Case:

### 1. Use Case User:
   - Register (Pendaftaran pengguna baru)
   - Login (Masuk ke akun pengguna)
   - Browse Products (Mencari produk)
   - View Product Details (Melihat detail produk)
   - Add to Cart (Menambahkan produk ke keranjang)
   - Place Order (Melakukan pemesanan)
   - Make Payment (Melakukan pembayaran)
   - Track Order (Melacak status pesanan)
   - Leave Product Review (Meninggalkan ulasan produk)

### 2. Use Case Admin:
   - Manage Products (Mengelola produk)
   - Manage Orders (Mengelola pesanan)
   - Manage Inventory (Mengelola stok barang)
   - Manage Users (Mengelola pengguna)
   - Generate Reports (Menghasilkan laporan)
   - Handle Customer Support (Menangani dukungan pelanggan)
   - Analyze Sales Data (Menganalisis data penjualan)
   - Monitor Site Performance (Memantau kinerja situs)
   - Set Promotions and Discounts (Menetapkan promosi dan diskon)

Peringkat Use Case di atas disusun berdasarkan pentingnya masing-masing Use Case. Use Case User mencakup aktivitas yang dilakukan oleh pengguna biasa, seperti mencari produk, melakukan pemesanan, dan melacak pesanan. Use Case Admin mencakup aktivitas yang dilakukan oleh admin Tokopedia untuk mengelola produk, pesanan, pengguna, dan melihat laporan penjualan.
