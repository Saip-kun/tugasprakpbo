# KONSEP DASAR OOP (Object-Oriented Programming)
Konsep dasar Object-Oriented Programming (OOP) adalah pendekatan dalam pemrograman yang berfokus pada objek-objek yang merepresentasikan entitas atau konsep dalam dunia nyata. OOP mengorganisir kode menjadi objek-objek yang memiliki atribut (data) dan metode (fungsi) yang terkait. Berikut adalah penjelasan konsep dasar OOP:

1. Kelas (Class):
   - Kelas adalah cetak biru (blueprint) untuk menciptakan objek. Itu mendefinisikan atribut-atribut (variabel) dan metode-metode (fungsi) yang dimiliki oleh objek.
   - Kelas berfungsi sebagai wadah untuk mendefinisikan karakteristik dan perilaku objek yang akan dibuat berdasarkan kelas tersebut.

2. Objek (Object):
   - Objek adalah instance atau perwujudan nyata dari sebuah kelas. Objek memiliki atribut-atribut unik yang membedakannya dari objek lainnya.
   - Sebagai contoh, jika kelas adalah "Mobil", maka objek-objeknya bisa menjadi "Mobil Merah" atau "Mobil Biru", dengan atribut-atribut yang berbeda.

3. Enkapsulasi (Encapsulation):
   - Enkapsulasi adalah konsep yang menggabungkan data (atribut) dan fungsi-fungsi (metode) yang beroperasi pada data ke dalam satu kesatuan yang terlindungi.
   - Dengan menggunakan enkapsulasi, objek dapat menyembunyikan implementasi internalnya dan hanya memberikan antarmuka (interface) yang terbatas untuk berinteraksi dengan objek tersebut.

4. Pewarisan (Inheritance):
   - Pewarisan memungkinkan kelas baru (kelas anak) untuk mewarisi atribut dan metode dari kelas yang sudah ada (kelas induk atau superkelas).
   - Dengan pewarisan, kelas anak dapat memiliki karakteristik tambahan atau memodifikasi perilaku yang diturunkan dari kelas induknya.
   - Pewarisan membantu dalam pembuatan hierarki kelas yang lebih terorganisir dan memungkinkan penggunaan kembali (reusability) kode.

5. Polimorfisme (Polymorphism):
   - Polimorfisme memungkinkan penggunaan metode yang sama dengan cara yang berbeda pada objek-objek yang berbeda.
   - Objek-objek yang berbeda dapat merespons metode dengan cara yang berbeda berdasarkan implementasi yang mereka miliki.
   - Polimorfisme memungkinkan fleksibilitas dalam pemrograman dan memungkinkan penanganan objek dengan cara yang lebih umum dan abstrak.

Konsep dasar OOP ini membantu dalam mengorganisir kode menjadi unit yang terpisah, memungkinkan penggunaan kembali kode, meningkatkan modularitas, memfasilitasi perubahan dan perbaikan, dan menyederhanakan pengembangan dan pemeliharaan aplikasi. OOP sering digunakan dalam bahasa pemrograman seperti Java, C++, dan Python untuk membangun aplikasi yang kompleks dan skalabel.
