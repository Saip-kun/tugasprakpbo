# Demonstrasi Penggunaan Inheritance (Pewarisan) dan Polymorphism (Polimorfisme) 
```HEWAN```
<pre><code>
class Hewan {
    private String nama;

    public Hewan(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void bersuara() {
        System.out.println("Hewan ini bersuara");
    }
}

class Kucing extends Hewan {
    public Kucing(String nama) {
        super(nama);
    }

    @Override
    public void bersuara() {
        System.out.println(getNama() + " mengeluarkan suara: Meow");
    }
}

class Anjing extends Hewan {
    public Anjing(String nama) {
        super(nama);
    }

    @Override
    public void bersuara() {
        System.out.println(getNama() + " mengeluarkan suara: Woof");
    }
}

public class InheritancePolymorphism {
    public static void main(String[] args) {
        Hewan hewan1 = new Kucing("Tom");
        Hewan hewan2 = new Anjing("Max");

        hewan1.bersuara();
        hewan2.bersuara();
    }
}</pre></code>


Dalam contoh di atas, kita memiliki kelas Hewan sebagai kelas induk (superclass). Kelas Hewan memiliki atribut nama dan metode bersuara() yang memberikan implementasi default "Hewan ini bersuara".

Kemudian, kita membuat dua kelas turunan (subclass) yaitu Kucing dan Anjing. Kedua kelas ini mewarisi sifat dan metode dari kelas Hewan. Masing-masing kelas turunan ini memberikan implementasi yang berbeda untuk metode bersuara() sesuai dengan sifat khusus mereka.

Di dalam metode main(), kita menciptakan objek hewan1 sebagai instansi dari kelas Kucing dan objek hewan2 sebagai instansi dari kelas Anjing. Meskipun objek tersebut dideklarasikan sebagai tipe Hewan, namun mereka tetap dapat menyimpan objek turunan yang sesuai.

Ketika kita memanggil metode bersuara() pada setiap objek, polymorphism memungkinkan pemanggilan metode yang sesuai dengan jenis objek yang sebenarnya. Dengan demikian, meskipun kita menggunakan tipe Hewan sebagai referensi, namun metode bersuara() yang dijalankan adalah versi yang diimplementasikan oleh kelas turunan masing-masing (Kucing dan Anjing).

Hasil output yang dihasilkan adalah:
```
Tom mengeluarkan suara: Meow
Max mengeluarkan suara: Woof
```

Dalam contoh ini, Inheritance memungkinkan kita untuk mewarisi sifat dan metode dari kelas induk (Hewan) ke kelas turunan (Kucing dan Anjing). Sedangkan Polymorphism memungkinkan kita untuk memperlakukan objek turunan dengan tipe kelas induk, dan pemanggilan metode akan disesuaikan dengan implementasi yang sesuai pada objek tersebut.
