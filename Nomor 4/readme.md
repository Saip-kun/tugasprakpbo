# Penjelasan Penggunaan Enkapsulasi
```Enkapsulasi```
<pre><code> public class Enkapsulasi {
    public static void main(String[] args) {
        Mahasiswa mahasiswa = new Mahasiswa();

        mahasiswa.setNama("teuku m saif");
        mahasiswa.setUsia(20);
        mahasiswa.setNim("1217050138");

        System.out.println("Nama: " + mahasiswa.getNama());
        System.out.println("Usia: " + mahasiswa.getUsia());
        System.out.println("NIM: " + mahasiswa.getNim());
    }
}

class Mahasiswa {
    private String nama;
    private int usia;
    private String nim;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUsia() {
        return usia;
    }

    public void setUsia(int usia) {
        this.usia = usia;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }
}</pre></code>

Output dari kode tersebut akan menjadi:

```
Nama: teuku m saif
Usia: 20
NIM: 1217050138
```

Kode di atas menggambarkan sebuah kelas `Mahasiswa` yang menggunakan enkapsulasi untuk mengatur atribut-atributnya (`nama`, `usia`, dan `nim`). Dalam metode `main`, kita membuat objek `mahasiswa` dari kelas `Mahasiswa` dan mengatur nilai atribut dengan menggunakan metode setter (`setNama()`, `setUsia()`, `setNim()`).

Kemudian, kita mencetak nilai atribut dengan menggunakan metode getter (`getNama()`, `getUsia()`, `getNim()`) dan mencetaknya sebagai output. Oleh karena itu, output yang dihasilkan sesuai dengan nilai yang kita berikan saat mengatur atribut melalui metode setter.
