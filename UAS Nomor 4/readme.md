# DESIGN PATTERN MVVM
MVVM (Model-View-ViewModel) adalah pola desain arsitektur perangkat lunak yang digunakan dalam pengembangan aplikasi dengan antarmuka pengguna (UI). Pola desain ini membantu memisahkan logika bisnis dari tampilan (UI) dengan menggunakan ViewModel sebagai penghubung antara Model (data) dan View (UI).

Berikut adalah penjelasan mengenai komponen utama dalam pola desain MVVM:

1. Model:
   Model mewakili data dan logika bisnis dalam aplikasi. Ini bisa berupa kelas yang merepresentasikan entitas bisnis, penyimpanan data, atau interaksi dengan server. Model tidak memiliki pengetahuan tentang View atau ViewModel, sehingga memungkinkan pemisahan yang jelas antara logika bisnis dan tampilan.

2. View:
   View adalah bagian yang menampilkan data dan berinteraksi dengan pengguna. Dalam konteks aplikasi Android, View dapat berupa Activity, Fragment, atau komponen UI lainnya. Dalam pola MVVM, View bertanggung jawab untuk menampilkan data yang disediakan oleh ViewModel dan menangani interaksi pengguna.

3. ViewModel:
   ViewModel bertindak sebagai perantara antara Model dan View. Ini berisi logika bisnis, mengelola state aplikasi, dan menyediakan data yang diperlukan oleh View. ViewModel tidak bergantung pada View secara langsung, sehingga dapat digunakan ulang di berbagai tampilan. ViewModel juga dapat berkomunikasi dengan Model untuk mengambil atau memperbarui data.

Selain komponen utama di atas, MVVM sering melibatkan penggunaan binding data untuk menghubungkan ViewModel dengan View. Binding data memungkinkan pembaruan otomatis antara ViewModel dan View, sehingga perubahan data di ViewModel akan langsung terlihat di tampilan.

Keuntungan dari pola desain MVVM antara lain:

- Memisahkan logika bisnis dan tampilan sehingga mudah untuk memahami, menguji, dan memelihara kode.
- Meningkatkan pengujian dengan memungkinkan pengujian unit pada ViewModel terlepas dari tampilan.
- Menghindari kode yang berantakan dengan menggunakan binding data untuk pembaruan otomatis antara ViewModel dan View.
- Membantu dalam pengembangan tim dengan memungkinkan pemisahan tugas antara pengembang UI dan pengembang logika bisnis.

## SCREENSHOT DESIGN
![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-11%20075610.png)
![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-11%20075549.png)
![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-11%20075400.png) ![image.png](https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-11%20075643.png)
