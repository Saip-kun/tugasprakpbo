# Penggunaan Abstraksi

```Abstraksi Mahasiswa```
<pre><code>public abstract class Mahasiswa {
    private String nama;
    private int usia;
    private String nim;

    public Mahasiswa(String nama, int usia, String nim) {
        this.nama = nama;
        this.usia = usia;
        this.nim = nim;
    }

    public abstract void belajar();

    public String getNama() {
        return nama;
    }

    public int getUsia() {
        return usia;
    }

    public String getNim() {
        return nim;
    }
}

public class MahasiswaS1 extends Mahasiswa {
    private String jurusan;

    public MahasiswaS1(String nama, int usia, String nim, String jurusan) {
        super(nama, usia, nim);
        this.jurusan = jurusan;
    }

    @Override
    public void belajar() {
        System.out.println("Mahasiswa S1 " + getNama() + " sedang belajar di jurusan " + jurusan);
    }
}

public class MahasiswaS2 extends Mahasiswa {
    private String programStudi;

    public MahasiswaS2(String nama, int usia, String nim, String programStudi) {
        super(nama, usia, nim);
        this.programStudi = programStudi;
    }

    @Override
    public void belajar() {
        System.out.println("Mahasiswa S2 " + getNama() + " sedang belajar di program studi " + programStudi);
    }
}

public class AbstractionExample {
    public static void main(String[] args) {
        Mahasiswa mahasiswaS1 = new MahasiswaS1("John Doe", 20, "123456", "Informatika");
        Mahasiswa mahasiswaS2 = new MahasiswaS2("Jane Smith", 22, "654321", "Manajemen");

        mahasiswaS1.belajar();
        mahasiswaS2.belajar();
    }
}</pre></code>


Dalam contoh di atas, kita menggunakan konsep Abstraksi untuk mendefinisikan kelas abstrak `Mahasiswa`, yang memiliki atribut nama, usia, dan NIM. Kelas `Mahasiswa` memiliki metode abstrak `belajar()` yang tidak memiliki implementasi di dalamnya.

Kemudian, kita membuat kelas `MahasiswaS1` dan `MahasiswaS2` yang merupakan turunan dari kelas `Mahasiswa`. Setiap kelas turunan tersebut mengimplementasikan metode `belajar()` sesuai dengan jenis mahasiswa yang bersangkutan.

Di dalam metode `main()`, kita menciptakan objek `mahasiswaS1` dan `mahasiswaS2` yang merupakan instansi dari kelas turunan. Kemudian, kita memanggil metode `belajar()` pada setiap objek.

Hasil output yang dihasilkan adalah:
```
Mahasiswa S1 John Doe sedang belajar di jurusan Informatika
Mahasiswa S2 Jane Smith sedang belajar di program studi Manajemen
```

Dalam contoh ini, Abstraksi memungkinkan kita untuk mendefinisikan kerangka umum yang digunakan oleh kelas-kelas turunannya, sementara detail implementasinya ditangani oleh kelas turunan. Dengan demikian, kita dapat menggambarkan konsep umum Mahasiswa dan membiarkan setiap jenis mahasiswa menentukan cara mereka belajar sesuai dengan program studi atau jurusan mereka masing-masing.
