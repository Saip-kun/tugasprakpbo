# SOLID DESIGN PRINCIPLE

Prinsip SOLID adalah kumpulan prinsip desain objek-oriented yang dirancang untuk membantu menghasilkan kode yang bersih, terstruktur, dan mudah dimengerti. Berikut adalah contoh dan penjelasan singkat tentang masing-masing prinsip SOLID dan bagaimana mereka dapat diterapkan dalam pengembangan Kotlin di Android Studio:

![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-10%20145836.png ) ![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-10%20145923.png )
![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-10%20145851.png ) 

## 1. Prinsip Single Responsibility (SRP):

   Prinsip ini menyatakan bahwa sebuah kelas seharusnya hanya memiliki satu alasan untuk berubah. Dalam konteks Android Studio, ini berarti bahwa setiap kelas sebaiknya hanya bertanggung jawab atas satu tugas spesifik. Misalnya, jika Anda membuat kelas untuk mengelola koneksi ke server, kelas tersebut seharusnya hanya fokus pada tugas tersebut dan tidak mencampuradukkan logika yang tidak terkait.

## 2. Prinsip Open-Closed (OCP):

   Prinsip ini menyatakan bahwa sebuah entitas (kelas, modul, dll.) seharusnya terbuka untuk perluasan (open) namun tertutup untuk modifikasi (closed).

## 3. Prinsip Liskov Substitution (LSP):
   Prinsip ini menyatakan bahwa objek dari kelas turunan seharusnya dapat digunakan sebagai pengganti objek dari kelas induk tanpa mengubah kebenaran atau kegunaan program. Dalam pengembangan Android, ini berarti bahwa ketika Anda membuat kelas turunan, kelas tersebut harus mematuhi kontrak dari kelas induknya dan tidak mengubah perilaku yang diharapkan.

## 4. Prinsip Interface Segregation (ISP):
   Prinsip ini menyatakan bahwa klien tidak boleh dipaksa untuk bergantung pada antarmuka yang tidak mereka gunakan. Dalam pengembangan Android, ini berarti bahwa Anda sebaiknya memisahkan antarmuka (interface) yang besar menjadi beberapa antarmuka yang lebih kecil dan spesifik, sehingga klien hanya perlu mengimplementasikan antarmuka yang relevan untuk mereka.

## 5. Prinsip Dependency Inversion (DIP):
   Prinsip ini menyatakan bahwa ketergantungan harus bergantung pada abstraksi, bukan implementasi. Dalam pengembangan Android, Anda dapat menerapkan prinsip ini dengan menggunakan pola desain seperti Pola Pengendali dan injeksi dependensi (dependency injection). Dengan injeksi dependensi, Anda mengurangi ketergantungan langsung antara kelas-kelas, sehingga membuat kode lebih fleksibel, teruji, dan mudah dimengerti.

SOLID bukanlah aturan yang kaku, tetapi lebih merupakan panduan untuk menghasilkan kode yang lebih mudah dikelola dan diubah. Dengan menerapkan prinsip-prinsip SOLID ini, Anda dapat meningkatkan struktur dan kualitas kode dalam pengembangan aplikasi Android menggunakan Kotlin di Android Studio.
